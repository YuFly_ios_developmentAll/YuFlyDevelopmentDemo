//
//  MyHeader.h
//  YuFlyDevelopmentDemo
//
//  Created by YuFly on 17/3/23.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#ifndef MyHeader_h
#define MyHeader_h


#endif /* MyHeader_h */
#define RGB(a,b,c) [UIColor colorWithRed:a/255.0 green:b/255.0 blue:c/255.0 alpha:1.0]
#define CellHeight 60
#define NavigationBarHeight 64
#define NavigationBarBgColor  [UIColor colorWithRed:63/255.0 green:98/255.0 blue:153/255.0 alpha:1.0]
#define WIDTH_SCREEN    [[[UIApplication sharedApplication] delegate] window].bounds.size.width
#define HEIGHT_SCREEN   [[[UIApplication sharedApplication] delegate] window].bounds.size.height

