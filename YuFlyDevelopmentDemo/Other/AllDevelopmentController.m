//
//  AllDevelopmentController.m
//  YuFlyDevelopmentDemo
//
//  Created by YuFly on 17/3/23.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import "AllDevelopmentController.h"

#import "TakePhotoAndVideoController.h"
#import "SelectedMenuMainVC.h"
#import "ImageZoomMainVC.h"
#import "TextChangePath.h"
#import "WebViewMainVC.h"
#import "DeviceInfoVC.h"

@interface AllDevelopmentController  ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *tb;
    NSMutableArray *dataArray;
}
@end
@implementation AllDevelopmentController
- (void)viewDidLoad{
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=YES;
}

- (void)createUI{
    self.view.backgroundColor=[UIColor whiteColor];
    [self initNavigationWithCenterTitle:@"ios开发集合"];
    [self.dataArray addObject:@"仿微信单击拍照长按录像功能"];
    [self.dataArray addObject:@"菜单栏"];
    [self.dataArray addObject:@"单击图片放大缩小"];
    [self.dataArray addObject:@"文字转路径"];
    [self.dataArray addObject:@"DSBridge桥接OC-JS"];
    [self.dataArray addObject:@"获取设备信息"];
    [self.view addSubview:self.tb];
}

- (NSMutableArray *)dataArray{
    if(!dataArray){
        dataArray=[NSMutableArray array];
    }
    return dataArray;
}

- (UITableView *)tb{
    if(!tb){
        tb=[[UITableView alloc]initWithFrame:CGRectMake(0,64, self.view.width, self.view.height-64) style:UITableViewStylePlain];
        tb.backgroundColor=RGB(255, 255, 255);
        tb.showsVerticalScrollIndicator=NO;
        tb.showsHorizontalScrollIndicator=NO;
        tb.delegate=self;
        tb.dataSource=self;
        tb.separatorColor=[UIColor clearColor];
    }
    return tb;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CellHeight;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier=@"allCell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell==nil){
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if(indexPath.row%2!=0){
        cell.backgroundColor = [UIColor whiteColor];
    }else{
        cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    cell.textLabel.text=[dataArray objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row==0){
        TakePhotoAndVideoController *ctrl=[[TakePhotoAndVideoController alloc]init];
        [self.navigationController pushViewController:ctrl animated:YES];
    }else if(indexPath.row==1){
        SelectedMenuMainVC *ctrl = [[SelectedMenuMainVC alloc]init];
        [self.navigationController pushViewController:ctrl animated:YES];
    }else if(indexPath.row==2){
        ImageZoomMainVC *ctrl = [[ImageZoomMainVC alloc]init];
        [self.navigationController pushViewController:ctrl animated:YES];
    }else if(indexPath.row==3){
        TextChangePath *ctrl = [[TextChangePath alloc]init];
        [self.navigationController pushViewController:ctrl animated:YES];
    }else if(indexPath.row==4){
        WebViewMainVC *vc = [[WebViewMainVC alloc]initWithResource:@"test" ofType:@"html"];
        [self.navigationController pushViewController:vc animated:YES];
    }else if(indexPath.row==5){
        DeviceInfoVC *vc = [[DeviceInfoVC alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

@end
