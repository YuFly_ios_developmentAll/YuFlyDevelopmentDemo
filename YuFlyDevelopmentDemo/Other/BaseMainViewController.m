//
//  BaseMainViewController.m
//  YuFlyDevelopmentDemo
//
//  Created by YuFly on 17/3/23.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import "BaseMainViewController.h"
@interface BaseMainViewController ()
{
    UILabel *mainTitleLabel;
}
@end
@implementation BaseMainViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
}

- (void)initNavigationWithLeftBtnBgImg:(UIImage *)leftBgImg
                          LeftBtnTitle:(NSString *)leftTitle
                           CenterTitle:(NSString *)title
                         RightBtnBgImg:(UIImage *)rightBgImg
                         RightBtnTitle:(NSString *)rightTitle{
    
    
    if(!mainTitleLabel){
        UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), NavigationBarHeight)];
        bgView.backgroundColor=NavigationBarBgColor;
        [self.view addSubview:bgView];
        
        mainTitleLabel = [self createLabelWithFrame:CGRectMake(0, 20, bgView.width, 44)
                                                    font:[UIFont boldSystemFontOfSize:18]
                                                    text:title
                                               textColor:[UIColor whiteColor]];
        [bgView addSubview:mainTitleLabel];
        mainTitleLabel.textAlignment = NSTextAlignmentCenter;
        
        UIButton *leftBtn= [[UIButton alloc]initWithFrame:CGRectMake(0,20,75, 44)];
        if(self.view.width==320){
            leftBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        }else{
            leftBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        }
        [leftBtn setTitleColor:RGB(255, 255, 255) forState:UIControlStateNormal];
        [leftBtn addTarget:self action:@selector(leftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:leftBtn];
        if(leftTitle)
            [leftBtn setTitle:@"返回" forState:UIControlStateNormal];
        if(leftBgImg)
            [leftBtn setImage:leftBgImg forState:UIControlStateNormal];
        if(leftBgImg==nil && leftTitle==nil)
            [leftBtn setHidden:YES];
        
        UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(self.view.width-50,20, 44, 44)];
        btn.tag=9;
        if(self.view.width==320){
            btn.titleLabel.font = [UIFont systemFontOfSize:14];
        }else{
            btn.titleLabel.font = [UIFont systemFontOfSize:16];
        }
        
        [btn setTitleColor:RGB(255, 255, 255) forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(rightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        if(rightBgImg)
            [btn setImage:rightBgImg forState:UIControlStateNormal];
        if(rightTitle)
            [btn setTitle:rightTitle forState:UIControlStateNormal];
        

    }
    mainTitleLabel.text=title;
}

- (void)initNavigationWithCenterTitle:(NSString *)title{
    [self initNavigationWithLeftBtnBgImg:nil LeftBtnTitle:nil CenterTitle:title RightBtnBgImg:nil RightBtnTitle:nil];
}

- (void)initNavigationWithLeftBtnBgImg:(UIImage *)leftBgImg
                           CenterTitle:(NSString *)title
                         RightBtnBgImg:(UIImage *)rightBgImg{
    [self initNavigationWithLeftBtnBgImg:leftBgImg LeftBtnTitle:nil CenterTitle:title RightBtnBgImg:rightBgImg RightBtnTitle:nil];
}

- (void)initNavigationWithLeftBtnBgImg:(UIImage *)leftBgImg
                           CenterTitle:(NSString *)title
                         RightBtnTitle:(NSString *)rightTitle{
    [self initNavigationWithLeftBtnBgImg:leftBgImg LeftBtnTitle:nil CenterTitle:title RightBtnBgImg:nil RightBtnTitle:rightTitle];
}

- (void)initNavigationWithLeftBtnBgImg:(UIImage *)leftBgImg
                          LeftBtnTitle:(NSString *)leftTitle
                           CenterTitle:(NSString *)title
                         RightBtnBgImg:(UIImage *)rightBgImg{
    [self initNavigationWithLeftBtnBgImg:leftBgImg LeftBtnTitle:leftTitle CenterTitle:title RightBtnBgImg:rightBgImg RightBtnTitle:nil];
}

- (void)initNavigationWithLeftBtnBgImg:(UIImage *)leftBgImg
                          LeftBtnTitle:(NSString *)leftTitle
                           CenterTitle:(NSString *)title
                         RightBtnTitle:(NSString *)rightTitle{
    [self initNavigationWithLeftBtnBgImg:leftBgImg LeftBtnTitle:leftTitle CenterTitle:title RightBtnBgImg:nil RightBtnTitle:rightTitle];
}

- (void)initNavigationWithLeftBtnBgImg:(UIImage *)leftBgImg
                          LeftBtnTitle:(NSString *)leftTitle
                           CenterTitle:(NSString *)title{
    [self initNavigationWithLeftBtnBgImg:leftBgImg LeftBtnTitle:leftTitle CenterTitle:title RightBtnBgImg:nil RightBtnTitle:nil];
}

-(UILabel*)createLabelWithFrame:(CGRect)frm
                           font:(UIFont*)font
                           text:(NSString*)txt
                      textColor:(UIColor*)clr{
    UILabel* label = [[UILabel alloc] initWithFrame:frm];
    label.font = font;
    label.text = txt;
    label.textColor = clr;
    label.backgroundColor = [UIColor clearColor];
    return label;
}

- (void)leftBtnClick:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnClick:(UIButton *)btn{}

@end
