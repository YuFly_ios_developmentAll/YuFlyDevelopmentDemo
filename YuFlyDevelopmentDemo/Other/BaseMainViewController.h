//
//  BaseMainViewController.h
//  YuFlyDevelopmentDemo
//
//  Created by YuFly on 17/3/23.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyHeader.h"
#import "UIView+Frame.h"
@interface BaseMainViewController : UIViewController

- (void)initNavigationWithLeftBtnBgImg:(UIImage *)leftBgImg
                          LeftBtnTitle:(NSString *)leftTitle
                           CenterTitle:(NSString *)title
                         RightBtnBgImg:(UIImage *)rightBgImg
                         RightBtnTitle:(NSString *)rightTitle;

- (void)initNavigationWithCenterTitle:(NSString *)title;

- (void)initNavigationWithLeftBtnBgImg:(UIImage *)leftBgImg
                           CenterTitle:(NSString *)title
                         RightBtnBgImg:(UIImage *)rightBgImg;

- (void)initNavigationWithLeftBtnBgImg:(UIImage *)leftBgImg
                           CenterTitle:(NSString *)title
                         RightBtnTitle:(NSString *)rightTitle;

- (void)initNavigationWithLeftBtnBgImg:(UIImage *)leftBgImg
                          LeftBtnTitle:(NSString *)leftTitle
                           CenterTitle:(NSString *)title
                         RightBtnBgImg:(UIImage *)rightBgImg;

- (void)initNavigationWithLeftBtnBgImg:(UIImage *)leftBgImg
                          LeftBtnTitle:(NSString *)leftTitle
                           CenterTitle:(NSString *)title
                         RightBtnTitle:(NSString *)rightTitle;

- (void)initNavigationWithLeftBtnBgImg:(UIImage *)leftBgImg
                          LeftBtnTitle:(NSString *)leftTitle
                           CenterTitle:(NSString *)title;
@end
