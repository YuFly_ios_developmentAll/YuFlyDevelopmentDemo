//
//  NSString(Utility).h
//  ShanHongZaiHaiPDA
//
//  Created by Shiqyn on 12-7-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(Utility)

//获取农历时间
+(NSString*)getChineseCalendarWithDate:(NSDate *)date;

//返回格式为2012.02.09格式的时间
+(NSString*)formatTimeString22FromNSDate:(NSDate*)theDate;

+(NSString*)formateTimeStringFrom:(NSString*)orginTimeString;

//+(int)getMonthDayStringFromTheNSDate:(NSDate*)date;
+(NSString*)getMonthDayStringFromTheNSDate:(NSDate*)date;

+(int)getMonthFromTheNSDate:(NSDate*)date;

+(int)getYearFromTimeString:(NSString*)timestring;

+(NSString*)getDateStringFromNSDate:(NSDate*)date withFormat:(NSString*)formatString;

//将NSDate转成@"05月06日\n12时"
+(NSString*)formatTimeStringFromNSDate:(NSDate*)theDate;

//返回格式为2012-02-09 13:12格式的时间
+(NSString*)formatTimeString2FromNSDate:(NSDate*)theDate;
//返回格式为2012-02-09 13:12:00格式的时间
+(NSString*)formatTimeStringYYRSFMFromNSDate:(NSDate*)theDate;

+(NSString*)formateTimeString10From:(NSString*)orginTimeString;

//将格式为2012-05-28 02:00转成2012年05月28日02时
+(NSString*)formateTimeString1From:(NSString*)orginTimeString;

//将格式为2012-05-28 02:00转成05/28 02:00
+(NSString*)formateTimeString11From:(NSString*)orginTimeString;

//将格式为2012-05-28 02:00转成05月28日\n02时
+(NSString*)formateTimeString2From:(NSString*)orginTimeString;

//将格式@"2012-01-29 08:00:00"转成12年01月29日08时
+(NSString*)formateTimeString3From:(NSString*)orginTimeString;

//将格式@"2012-01-29 08:00:00"转成08:00
+(NSString*)formateTimeString13From:(NSString*)orginTimeString;

//将格式@"2012-01-29 08:00:00"转成08时00分
+(NSString*)formateTimeString113From:(NSString*)orginTimeString;

//将格式@"2012-01-29 08:00:00"转成2014年01月29日
+(NSString*)formateTimeString114From:(NSString*)orginTimeString;

//将格式@"2012-01-29 08:00:00"转成01月29日08时
+(NSString*)formateTimeString9From:(NSString*)orginTimeString;


//将格式为2012-05-28 02:00转成05月28日02时
+(NSString*)formateTimeString4From:(NSString*)orginTimeString;

//将格式为2012-05-28 02:00转成05月28日
+(NSString*)formateTimeString5From:(NSString*)orginTimeString;

//将格式@"2012-01-29 08:00:00"转成 "29日08时"
+(NSString*)formateTimeString12From:(NSString*)orginTimeString;

+(NSString*)formatTimeString3FromNSDate:(NSDate*)theDate;

//将格式为2012-05-28 02:00转成2012年05月28日02时
+(NSString*)formateTimeString6From:(NSString*)orginTimeString;

//将格式为2012-05-28 02:00转成2012年05月28日 02时
+(NSString*)formateTimeString16From:(NSString*)orginTimeString;

//将格式为2012-05-28 02:00转成2012年05月28日 02时
+(NSString*)formateTimeString17From:(NSString*)orginTimeString;

+(NSDate*)stringToDate:(NSString*)dateStr andDateFormat:(NSString*)dateFormatString setUTC:(BOOL)b;
+(NSDate*)stringToDate:(NSString*)dateStr andDateFormat:(NSString*)dateFormatString;

+(NSInteger)getTheHourFromTheNSDate:(NSDate*)theDate;


//将20120102182020格式化成@"2000-01-01 00:00:00"
+(NSString*)formatTimeStringToStrongsoftServerFormat:(NSString*)originTimeString;
@end
