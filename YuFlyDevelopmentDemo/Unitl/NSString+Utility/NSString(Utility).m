//
//  NSString(Utility).m
//  ShanHongZaiHaiPDA
//
//  Created by Shiqyn on 12-7-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "NSString(Utility).h"

@implementation NSString(Utility)
+(NSString*)getChineseCalendarWithDate:(NSDate *)date{
    
    NSArray *chineseYears = [NSArray arrayWithObjects:
                             @"甲子", @"乙丑", @"丙寅",    @"丁卯",    @"戊辰",    @"己巳",    @"庚午",    @"辛未",    @"壬申",    @"癸酉",
                             @"甲戌",    @"乙亥",    @"丙子",    @"丁丑", @"戊寅",    @"己卯",    @"庚辰",    @"辛己",    @"壬午",    @"癸未",
                             @"甲申",    @"乙酉",    @"丙戌",    @"丁亥",    @"戊子",    @"己丑",    @"庚寅",    @"辛卯",    @"壬辰",    @"癸巳",
                             @"甲午",    @"乙未",    @"丙申",    @"丁酉",    @"戊戌",    @"己亥",    @"庚子",    @"辛丑",    @"壬寅",    @"癸丑",
                             @"甲辰",    @"乙巳",    @"丙午",    @"丁未",    @"戊申",    @"己酉",    @"庚戌",    @"辛亥",    @"壬子",    @"癸丑",
                             @"甲寅",    @"乙卯",    @"丙辰",    @"丁巳",    @"戊午",    @"己未",    @"庚申",    @"辛酉",    @"壬戌",    @"癸亥", nil];
    
    NSArray *chineseMonths=[NSArray arrayWithObjects:
                            @"正月", @"二月", @"三月", @"四月", @"五月", @"六月", @"七月", @"八月",
                            @"九月", @"十月", @"冬月", @"腊月", nil];
    
    
    NSArray *chineseDays=[NSArray arrayWithObjects:
                          @"初一", @"初二", @"初三", @"初四", @"初五", @"初六", @"初七", @"初八", @"初九", @"初十",
                          @"十一", @"十二", @"十三", @"十四", @"十五", @"十六", @"十七", @"十八", @"十九", @"廿十",
                          @"廿一", @"廿二", @"廿三", @"廿四", @"廿五", @"廿六", @"廿七", @"廿八", @"廿九", @"三十",  nil];
    
    
    NSCalendar *localeCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierChinese];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    
    NSDateComponents *localeComp = [localeCalendar components:unitFlags fromDate:date];

    NSString *y_str = [chineseYears objectAtIndex:localeComp.year-1];
    NSString *m_str = [chineseMonths objectAtIndex:localeComp.month-1];
    NSString *d_str = [chineseDays objectAtIndex:localeComp.day-1];
    
    NSString *chineseCal_str =[NSString stringWithFormat: @"%@年%@%@",y_str,m_str,d_str];
    return chineseCal_str;
}

+(NSString*)formatTimeStringFromNSDate:(NSDate*)theDate
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *comps  = [calendar components:unitFlags fromDate:theDate];  
     
    NSInteger month = [comps month];
    NSInteger day = [comps day];
    NSInteger hour = [comps hour];
    
    return [NSString stringWithFormat:@"%ld月%ld日\n%ld时", (long)month, (long)day, (long)hour];
 }

+(NSString*)formatTimeString2FromNSDate:(NSDate*)theDate
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
     //[calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *comps  = [calendar components:unitFlags fromDate:theDate];  
    
    NSInteger year = [comps year];
    NSInteger month = [comps month];
    NSInteger day = [comps day];
    NSInteger hour = [comps hour];
    NSInteger minute = [comps minute];
    
    return [NSString stringWithFormat:@"%ld-%ld-%ld %ld:%ld",(long)year, (long)month, (long)day, (long)hour,(long)minute];
}

+(NSString*)formatTimeStringYYRSFMFromNSDate:(NSDate*)theDate
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    //[calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *comps  = [calendar components:unitFlags fromDate:theDate];
    
    NSInteger year = [comps year];
    NSInteger month = [comps month];
    NSInteger day = [comps day];
    NSInteger hour = [comps hour];
    NSInteger minute = [comps minute];
    NSInteger second=[comps second];
    return [NSString stringWithFormat:@"%ld-%ld-%ld %ld:%ld:%ld",(long)year, (long)month, (long)day, (long)hour,(long)minute,(long)second];
}

+(NSString*)formatTimeString22FromNSDate:(NSDate*)theDate
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    //[calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *comps  = [calendar components:unitFlags fromDate:theDate];
    
    NSInteger year = [comps year];
    NSInteger month = [comps month];
    NSInteger day = [comps day];
    
    return [NSString stringWithFormat:@"%ld.%ld.%ld",(long)year, (long)month, (long)day];
}

+(NSInteger)getTheHourFromTheNSDate:(NSDate*)theDate
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSInteger unitFlags =  NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *comps  = [calendar components:unitFlags fromDate:theDate];
     
    NSInteger hour = [comps hour];
    
    return  hour;
}

+(NSString*)getDateStringFromNSDate:(NSDate*)date withFormat:(NSString*)formatString
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSInteger unitFlags =  NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *comps  = [calendar components:unitFlags fromDate:date];  
   
    NSInteger year = [comps year];
    NSInteger month = [comps month];
    NSInteger day = [comps day];
    NSInteger hour = [comps hour];
    NSInteger min = [comps minute];
    NSInteger sec = [comps second];  
    
    return [NSString stringWithFormat:formatString, year, month, day, hour, min, sec];
}

//将格式为2012-05-28 02:00转成05/28 02:00
+(NSString*)formateTimeString11From:(NSString*)orginTimeString{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" -T:"]];
    if([array count]>=4 )
    {
        return [NSString stringWithFormat:@"%02d/%02d %02d:%02d",  [[array objectAtIndex:1] intValue]
                , [[array objectAtIndex:2] intValue]
                , [[array objectAtIndex:3] intValue],
                [[array objectAtIndex:4] intValue]];
    }
    else
        return @"";
    
    return @"";
}

//将格式为2012-05-28 02:00转成2012年05月28日 02:00
+(NSString*)formateTimeStringFrom:(NSString*)orginTimeString{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" -T:"]];
    if([array count]>=5 )
    {
        return [NSString stringWithFormat:@"%02d年%02d月%02d日 %02d:%02d:%02d",  [[array objectAtIndex:0] intValue], [[array objectAtIndex:1] intValue]
                , [[array objectAtIndex:2] intValue]
                , [[array objectAtIndex:3] intValue],
                [[array objectAtIndex:4] intValue],
                [[array objectAtIndex:5] intValue]];
    }
    else
        return @"";
    
    return @"";
}

+(NSString*)formateTimeString1From:(NSString*)orginTimeString
{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" -T:"]];
    if([array count]>=4 )
    {
        return [NSString stringWithFormat:@"%@年%02d月%02d日%02d时", [array objectAtIndex:0], [[array objectAtIndex:1] intValue]
                , [[array objectAtIndex:2] intValue]
                , [[array objectAtIndex:3] intValue]];
    }
    else 
        return @"";
    
    return @"";
}
+(NSString*)formateTimeString6From:(NSString*)orginTimeString
{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"- :"]];
    if([array count]>=4 )
    {
        return [NSString stringWithFormat:@"%@年%02d月%02d日%02d时%02d分", [array objectAtIndex:0], [[array objectAtIndex:1] intValue]
                , [[array objectAtIndex:2] intValue]
                , [[array objectAtIndex:3] intValue]
                , [[array objectAtIndex:4] intValue]];
    }
    else
        return @"";
    
    return @"";
}
//将格式为2012-05-28 02:00转成2012年05月28日 02时
+(NSString*)formateTimeString16From:(NSString*)orginTimeString{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"- :"]];
    if([array count]>=5 )
    {
        return [NSString stringWithFormat:@"%@年%02d月%02d日 %02d时",
                [array objectAtIndex:0], [[array objectAtIndex:1] intValue]
                , [[array objectAtIndex:2] intValue]
                , [[array objectAtIndex:3] intValue]];
    }
    else
        return @"";
    
    return @"";
}
//将格式为2012-05-28 02:00转成2012年05月28日 02时
+(NSString*)formateTimeString17From:(NSString*)orginTimeString{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"- :"]];
    if([array count]>=5 )
    {
        return [NSString stringWithFormat:@"%@年%02d月%02d日 %02d时",
                [array objectAtIndex:0], [[array objectAtIndex:1] intValue]
                , [[array objectAtIndex:2] intValue]
                , [[array objectAtIndex:3] intValue]];
    }
    else
        return @"";
    
    return @"";
}


+(NSString*)formateTimeString10From:(NSString*)orginTimeString
{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"- :"]];
    if([array count]>=4 )
    {
        return [NSString stringWithFormat:@"%02d月%02d日%02d时%02d分", [[array objectAtIndex:1] intValue]
                , [[array objectAtIndex:2] intValue]
                , [[array objectAtIndex:3] intValue]
                , [[array objectAtIndex:4] intValue]];
    }
    else
        return @"";
    
    return @"";
}

+(NSString*)formateTimeString2From:(NSString*)orginTimeString
{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"- :"]];
    if([array count]>=4 )
    {
        return [NSString stringWithFormat:@"%02d月%02d日\n%02d时", [[array objectAtIndex:1] intValue]
                , [[array objectAtIndex:2] intValue]
                , [[array objectAtIndex:3] intValue]];
    }
    else 
        return @"";
    
    return @"";
}

+(NSString*)formateTimeString4From:(NSString*)orginTimeString
{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"- :"]];
    if([array count]>=4 )
    {
        return [NSString stringWithFormat:@"%02d月%02d日%02d时", [[array objectAtIndex:1] intValue]
                , [[array objectAtIndex:2] intValue]
                , [[array objectAtIndex:3] intValue]];
    }
    else 
        return @"";
    
    return @"";
}

+(NSString*)formateTimeString5From:(NSString*)orginTimeString
{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"- :"]];
    if([array count]>=4 )
    {
        return [NSString stringWithFormat:@"%02d月%02d日", [[array objectAtIndex:1] intValue]
                , [[array objectAtIndex:2] intValue]];
    }
    else
        return @"";
    
    return @"";
}
+(int)getYearFromTimeString:(NSString*)timestring{
    NSArray* array = [timestring componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"- :"]];
    if(array.count)
        return [[array firstObject] intValue];
    return 0;
}
//将格式@"2012-01-29 08:00:00"转成08时00分
+(NSString*)formateTimeString113From:(NSString*)orginTimeString{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"- :"]];
    if([array count]>=6 )
    {
//        NSString* year = [array objectAtIndex:0];
//        NSString* shotYearString  ;
//        if(year.length >= 4)
//            shotYearString = [year substringFromIndex:2];
//        else
//            shotYearString = @"";
        
        return [NSString stringWithFormat:@"%02d时%02d分",
                [[array objectAtIndex:3] intValue]
                , [[array objectAtIndex:4] intValue]];
    }
    else
        return @"";
    
    return @"";
}
//将格式@"2012-01-29 08:00:00"转成2014年01月29里
+(NSString*)formateTimeString114From:(NSString*)orginTimeString{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"- :"]];
    if([array count]>=4 ){
        return [NSString stringWithFormat:@"%@年%02d月%02d日", [array objectAtIndex:0], [[array objectAtIndex:1] intValue]
                , [[array objectAtIndex:2] intValue]];
    }
    else
        return @"";
    
    return @"";
}

//将格式@"2012-01-29 08:00:00"转成08:00
+(NSString*)formateTimeString13From:(NSString*)orginTimeString{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"- :"]];
    if([array count]>=6 )
    {
        NSString* year = [array objectAtIndex:0];
        NSString* shotYearString  ;
        if(year.length >= 4)
            shotYearString = [year substringFromIndex:2];
        else
            shotYearString = @"";
        
        return [NSString stringWithFormat:@"%02d:%02d",
                [[array objectAtIndex:3] intValue]
                , [[array objectAtIndex:4] intValue]];
    }
    else
        return @"";
    
    return @"";
}
+(NSString*)formateTimeString3From:(NSString*)orginTimeString
{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"- :"]];
    if([array count]>=4 )
    {
        NSString* year = [array objectAtIndex:0];
        NSString* shotYearString  ;
        if(year.length >= 4)
        	shotYearString = [year substringFromIndex:2];
        else 
            shotYearString = @"";
        
        return [NSString stringWithFormat:@"%@年%02d月%02d日%02d时", shotYearString,
                						[[array objectAtIndex:1] intValue]
               							 , [[array objectAtIndex:2] intValue]
                							, [[array objectAtIndex:3] intValue]];
    }
    else 
        return @"";
    
    return @"";
}
+(NSString*)formateTimeString9From:(NSString*)orginTimeString{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"- :"]];
    if([array count]>=4 )
    {
        NSString* year = [array objectAtIndex:0];
        NSString* shotYearString  ;
        if(year.length >= 4)
            shotYearString = [year substringFromIndex:2];
        else
            shotYearString = @"";
        
        return [NSString stringWithFormat:@"%02d月%02d日%02d时",
                [[array objectAtIndex:1] intValue]
                , [[array objectAtIndex:2] intValue]
                , [[array objectAtIndex:3] intValue]];
    }
    else
        return @"";
    
    return @"";
}

//将20120102182020格式化成@"2000-01-01 00:00:00"
+(NSString*)formatTimeStringToStrongsoftServerFormat:(NSString*)originTimeString
{
    if([originTimeString length]>=12)
    {
        //NSString* year = [timeString substringToIndex:4];
        NSRange range;
        
        range.length = 4;
        range.location = 0;
        NSString* year = [originTimeString substringWithRange:range];
        
        range.length = 2;
        range.location = 4;
        NSString* month = [originTimeString substringWithRange:range];
        
        range.location = 6;
        NSString* day = [originTimeString substringWithRange:range];
        
        range.location = 8;
        NSString* hour = [originTimeString substringWithRange:range];
        
        range.location = 10;
        NSString* minute = [originTimeString substringWithRange:range];
        
        NSString* tmpString = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:00", year, month, day, hour, minute];
        return tmpString;
    }
    else
        return @"";
}

+(int)getMonthFromTheNSDate:(NSDate*)date{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *comps  = [calendar components:unitFlags fromDate:date];
    
    int month = (int)[comps month];
    
    return month;
}


+(NSString*)getMonthDayStringFromTheNSDate:(NSDate*)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *comps  = [calendar components:unitFlags fromDate:date];
    
    int month = (int)[comps month];
    int day = (int)[comps day];
    
    return [NSString stringWithFormat:@"%02d月/%02d日", month, day] ;
}

+(NSDate*)stringToDate:(NSString*)dateStr andDateFormat:(NSString*)dateFormatString
{
    return [self stringToDate:dateStr andDateFormat:dateFormatString setUTC:YES];
    
}
+(NSDate*)stringToDate:(NSString*)dateStr andDateFormat:(NSString*)dateFormatString setUTC:(BOOL)b
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if(b)
    	[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];  
    
    [dateFormatter  setDateFormat:dateFormatString];//@"yyyy-MM-dd HH:mm:ss"];    
    
    
    NSDate* dateFromString = [dateFormatter dateFromString:dateStr];    
    
    return dateFromString; 
}
//将格式@"2012-01-29 08:00:00"转成 "29日08时"
+(NSString*)formateTimeString12From:(NSString*)orginTimeString{
    NSArray* array = [orginTimeString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"- :"]];
    if(array.count>=6){
        return [NSString stringWithFormat:@"%02d日\n%02d时",[[array objectAtIndex:2] intValue],
                [[array objectAtIndex:3] intValue]];
    }
    return @"";
}

//将NSDate转成@"05-06\n12:12"
+(NSString*)formatTimeString3FromNSDate:(NSDate*)theDate
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *comps  = [calendar components:unitFlags fromDate:theDate];
    
    int month = (int)[comps month];
    int day = (int)[comps day];
    int hour = (int)[comps hour];
    int minute = (int)[comps minute];
    
    return [NSString stringWithFormat:@"%02d-%02d\n%02d:%02d", month, day, hour, minute];
}


@end
