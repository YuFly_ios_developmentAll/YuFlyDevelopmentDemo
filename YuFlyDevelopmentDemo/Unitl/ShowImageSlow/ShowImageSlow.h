//
//  ShowImageSlow.h
//  ShowImgeSlow
//
//  Created by Ibokan on 13-5-20.
//  Copyright (c) 2013年 YangFeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface ShowImageSlow : UIImageView
{
    int nowHeight;
    int endHeight;
    NSTimer *timer;
    CALayer *myLayer;
    
    BOOL isAdding;
}
@property (strong, nonatomic)UIImage *img;
@end
