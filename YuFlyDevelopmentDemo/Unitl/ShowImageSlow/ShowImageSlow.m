//
//  ShowImageSlow.m
//  ShowImgeSlow
//
//  Created by Ibokan on 13-5-20.
//  Copyright (c) 2013年 YangFeng. All rights reserved.
//

#import "ShowImageSlow.h"

@implementation ShowImageSlow

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        nowHeight = 0;
        self.frame  = frame;
        isAdding = NO;
    }
    return self;
}
-(void)setImage:(UIImage *)image
{
    
    if (isAdding) {
        [timer invalidate];
        [myLayer removeFromSuperlayer];
        myLayer =nil;
        isAdding = NO;
        nowHeight = 0;
        [super setImage:self.img];
    }
    self.img=[self getSubImage:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) image:image];
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(0, 0, self.frame.size.width, nowHeight);
    layer.contents= (id)self.img.CGImage;
    layer.masksToBounds =YES;
    layer.contentsGravity = kCAGravityBottom;
    
    myLayer = layer;
    [self.layer addSublayer:myLayer];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(addFrameHeight) userInfo:nil repeats:YES];
    [timer fire];
    isAdding = YES;
    
}
-(void)addFrameHeight
{
    if (self.superview) {
        nowHeight+=5;
        myLayer.frame = CGRectMake(0, 0, self.frame.size.width, nowHeight);
        if (nowHeight>= self.frame.size.height) {
            [timer invalidate];
            [super setImage:self.img];
            [myLayer removeFromSuperlayer];
            myLayer =nil;
            nowHeight=0;
            isAdding =NO;
        }
        
    }
    
}

-(UIImage*)getSubImage:(CGRect)rect image:(UIImage *)image
{
    CGImageRef subImageRef = CGImageCreateWithImageInRect(image.CGImage, rect);
    CGRect smallBounds = CGRectMake(0, 0, CGImageGetWidth(subImageRef), CGImageGetHeight(subImageRef));
    
    UIGraphicsBeginImageContext(smallBounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawImage(context, smallBounds, subImageRef);
    UIImage* smallImage = [UIImage imageWithCGImage:subImageRef];
    UIGraphicsEndImageContext();
    
    return smallImage;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
