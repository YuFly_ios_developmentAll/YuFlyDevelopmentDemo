//
//  ImageZoomView.h
//  YuFlyDevelopmentDemo
//
//  Created by YuFly on 17/8/17.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageZoomView : UIView
- (instancetype)initWithFrame:(CGRect)frame
                        ImageView:(UIImageView *)imgView;
@end
