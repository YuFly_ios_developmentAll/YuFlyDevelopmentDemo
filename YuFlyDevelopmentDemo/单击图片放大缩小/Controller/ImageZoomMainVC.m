//
//  ImageZoomMainVC.m
//  YuFlyDevelopmentDemo
//
//  Created by YuFly on 17/8/17.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import "ImageZoomMainVC.h"
#import "ShowImageSlow.h"
#import "ImageZoomView.h"
@interface ImageZoomMainVC ()
{
    ShowImageSlow *imageView;
}
@end

@implementation ImageZoomMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initNavigationWithLeftBtnBgImg:[UIImage imageNamed:@"返回白.png"] LeftBtnTitle:@"返回" CenterTitle:@"单击图片放大缩小"];
    [self create];
}

- (void)create{
    
    imageView = [[ShowImageSlow alloc]initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame)/4, CGRectGetHeight(self.view.frame)/4, CGRectGetWidth(self.view.frame)/2, CGRectGetHeight(self.view.frame)/2)];
    imageView.contentMode=UIViewContentModeScaleAspectFit;
    imageView.image=[UIImage imageNamed:@"xiegui1.jpg"];
    [self.view addSubview:imageView];
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    imageView.userInteractionEnabled=YES;
    [imageView addGestureRecognizer:singleTapGestureRecognizer];

  
}

- (void)singleTap:(UITapGestureRecognizer *)tap{
    //点击事件
    ImageZoomView *img=[[ImageZoomView alloc]initWithFrame:CGRectMake(0, 0, WIDTH_SCREEN, HEIGHT_SCREEN) ImageView:imageView];
    //当前视图
    [[UIApplication sharedApplication].keyWindow addSubview:img];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
