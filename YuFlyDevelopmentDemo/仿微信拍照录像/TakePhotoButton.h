//
//  TakePhotoButton.h
//  YuFlyDevelopmentDemo
//
//  Created by YuFly on 17/3/23.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TakePhotoButton : UIView
- (instancetype)initWithFrame:(CGRect)frame;
@end
