//
//  TakePhotoButton.m
//  YuFlyDevelopmentDemo
//
//  Created by YuFly on 17/3/23.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import "TakePhotoButton.h"
@interface TakePhotoButton ()
{
    UILabel *titleLabel;
    UIVisualEffectView *takePhotoBgView;
    UIView *cancelBgView;
    UIView *takePhotoBtn;
    UIButton *cancelBtn;
    UIButton *sureBnt;
    UIButton *backBtn;
    CGFloat height;
}
@end
@implementation TakePhotoButton
- (instancetype)initWithFrame:(CGRect)frame{
    if(self=[super initWithFrame:frame]){
        self.clipsToBounds=YES;
        height=frame.size.height-70;
        titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0,0, frame.size.width, 20)];
        titleLabel.text=@"轻触拍照，按住摄像";
        titleLabel.textColor=[UIColor whiteColor];
        titleLabel.textAlignment=NSTextAlignmentCenter;
        titleLabel.font=[UIFont systemFontOfSize:14];
        [self addSubview:titleLabel];
        cancelBgView=[[UIView alloc] initWithFrame:CGRectMake(frame.size.width/2-height/2, 25, height, height)];
        cancelBgView.layer.cornerRadius=height/2;
        cancelBgView.clipsToBounds=YES;
        [self addSubview:cancelBgView];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        effectView.alpha = 0.4f;
        effectView.frame = cancelBgView.bounds;
        [cancelBgView addSubview:effectView];
        cancelBgView.hidden=YES;
        cancelBtn=[[UIButton alloc]initWithFrame:CGRectMake(0,0, height, height)];
        cancelBtn.backgroundColor=[UIColor clearColor];
        [cancelBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        cancelBtn.layer.cornerRadius=height/2;
        cancelBtn.tag=1;
        [cancelBgView addSubview:cancelBtn];
        
        sureBnt=[[UIButton alloc]initWithFrame:CGRectMake(frame.size.width/2-height/2, 25, height, height)];
        sureBnt.backgroundColor=[UIColor whiteColor];
        [sureBnt addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        sureBnt.layer.cornerRadius=height/2;
        sureBnt.tag=2;
        sureBnt.hidden=YES;
        [self addSubview:sureBnt];

        takePhotoBgView=[[UIVisualEffectView alloc] initWithEffect:blurEffect];
        takePhotoBgView.frame=CGRectMake(frame.size.width/2-height/2, 25, height, height);
        takePhotoBgView.alpha=1.0;
//        takePhotoBgView.backgroundColor=[UIColor whiteColor];
        takePhotoBgView.layer.cornerRadius=height/2;
        takePhotoBgView.clipsToBounds=YES;
        [self addSubview:takePhotoBgView];
        
        
        takePhotoBtn=[[UIView alloc]initWithFrame:CGRectMake(10, 10, height-20, height-20)];
        takePhotoBtn.backgroundColor=[UIColor whiteColor];
        takePhotoBtn.layer.cornerRadius=(height-20)/2;
        [takePhotoBgView addSubview:takePhotoBtn];
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(takePhoto:)];
        takePhotoBtn.userInteractionEnabled=YES;
        tap.numberOfTapsRequired=1;
        [takePhotoBtn addGestureRecognizer:tap];
        UILongPressGestureRecognizer *longGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(takeVideo:)];
        [takePhotoBtn addGestureRecognizer:longGestureRecognizer];
        
        
        backBtn=[[UIButton alloc]initWithFrame:CGRectMake(frame.size.width/4-15, frame.size.height/2-15, 30, 30)];
        backBtn.tag=3;
        [backBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:backBtn];
        
    }
    return self;
}

- (void)takePhoto:(UITapGestureRecognizer *)gestureRecognizer{
    [self takePhotoSureAnimation];
}

- (void)takeVideo:(UILongPressGestureRecognizer *)gestureRecognizer{
    if(gestureRecognizer.state==UIGestureRecognizerStateBegan){
    
    }else if(gestureRecognizer.state==UIGestureRecognizerStateChanged){
        [self takeVideoBeganAnimation];
    }else if(gestureRecognizer.state==UIGestureRecognizerStateEnded){
        [self takeVideoEndAnimation];
    }
}

- (void)btnClick:(UIButton *)sender{
    if(sender.tag==1){
        [self takePhotoCancleAnimation];
    }else if(sender.tag==2){
    
    }else if(sender.tag==3){
        
    }
}

- (void)takePhotoSureAnimation{
        backBtn.hidden=YES;
        takePhotoBgView.hidden=YES;
        cancelBgView.hidden=NO;
        sureBnt.hidden=NO;
    [UIView animateWithDuration:0.2 animations:^{
        cancelBgView.frame=CGRectMake(self.frame.size.width/4-height/2, 25, height, height);
        sureBnt.frame=CGRectMake(self.frame.size.width*3/4-height/2, 25, height, height);
        
    } completion:^(BOOL finished) {
        
    }];
}

- (void)takePhotoCancleAnimation{
    
    [UIView animateWithDuration:0.2 animations:^{
        cancelBgView.frame=CGRectMake(self.frame.size.width/2-height/2, 25, height, height);
        sureBnt.frame=CGRectMake(self.frame.size.width/2-height/2, 25, height, height);
    } completion:^(BOOL finished) {
        backBtn.hidden=NO;
        takePhotoBgView.hidden=NO;
        cancelBgView.hidden=YES;
        sureBnt.hidden=YES;
    }];
    
}


- (void)takeVideoBeganAnimation{
    [UIView animateWithDuration:0.2 animations:^{
        takePhotoBgView.frame=CGRectMake(self.frame.size.width/2-height/2-5,20, height+10, height+10);
        takePhotoBgView.layer.cornerRadius=(height+10)/2;
        takePhotoBtn.frame=CGRectMake(20, 20, height-30, height-30);
        takePhotoBtn.layer.cornerRadius=(height-30)/2;
    }];
}

- (void)takeVideoEndAnimation{
    __weak typeof(self) weakself=self;
    [UIView animateWithDuration:0.2 animations:^{
        takePhotoBgView.frame=CGRectMake(self.frame.size.width/2-height/2, 25, height, height);
        takePhotoBgView.layer.cornerRadius=(height)/2;
        takePhotoBtn.frame=CGRectMake(10, 10, height-20, height-20);
        takePhotoBtn.layer.cornerRadius=(height-20)/2;
    } completion:^(BOOL finished) {
        [weakself takePhotoSureAnimation];
    }];
}

@end
