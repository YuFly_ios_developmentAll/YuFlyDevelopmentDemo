//
//  WebViewMainVC.m
//  DSBridgeTest
//
//  Created by YuFly on 2018/2/1.
//  Copyright © 2018年 YuFly. All rights reserved.
//

#import "WebViewMainVC.h"
#import "dsbridge.h"
#import "JSObject.h"
typedef NS_ENUM(NSInteger,RequestType) {
    RequestTypeURL=0,
    RequestTypeHtml
};
@interface WebViewMainVC ()<DWebviewDelegate>
{
    NSURLRequest *                                              urlRequest;
    NSString *                                                  htmlContent;
    JSObject *                                                  jsObject;
    UIActivityIndicatorView *                                   activityIndicator;
}
@property (assign, nonatomic)RequestType                        requestType;
@property (strong, nonatomic)DWebview *                         webView;
@end

@implementation WebViewMainVC

- (instancetype)initWithURLString:(NSString *)urlstring{
    if(self=[super init]){
        self.requestType = RequestTypeURL;
        urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlstring]];
    }
    return self;
}

- (instancetype)initWithResource:(NSString *)name ofType:(NSString *)type{
    if(self=[super init]){
        self.requestType = RequestTypeHtml;
        NSString * htmlPath = [[NSBundle mainBundle] pathForResource:name ofType:type];
        htmlContent = [NSString stringWithContentsOfFile:htmlPath encoding:NSUTF8StringEncoding error:nil];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self initNavigationWithLeftBtnBgImg:[UIImage imageNamed:@"返回白@2x.png"] LeftBtnTitle:@"返回" CenterTitle:@"DSBridge"];
    [self.view addSubview:self.webView];
    [self.view addSubview:self.activityIndicator];
}

- (DWebview *)webView{
    if(!_webView){
        __weak typeof(&*self)  ws =self;
        _webView = [[DWebview alloc]initWithFrame:CGRectMake(0, 64, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-64)];
        _webView.backgroundColor = [UIColor whiteColor];
        _webView.delegate=self;
        jsObject = [[JSObject alloc]init];
        jsObject.goBack = ^(NSInteger type) {
            [ws goBack:type];
        };
        _webView.JavascriptInterfaceObject=jsObject;
        
        if(self.requestType==RequestTypeURL){
            [_webView loadRequest:urlRequest];
        }else if(self.requestType==RequestTypeHtml){
            [_webView loadHTMLString:htmlContent baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
        }
        //获取js数据api
        [_webView setJavascriptContextInitedListener:^(){
            [ws.webView callHandler:@"getJSDataMethod"
                        arguments:[[NSArray alloc] initWithObjects:@1,@"hello", nil]
                completionHandler:^(NSString * value){
                    
                }];
        }];
        
    }
    return _webView;
}

- (UIActivityIndicatorView *)activityIndicator{
    if(!activityIndicator){
        activityIndicator=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.center = CGPointMake(CGRectGetWidth(self.view.frame)/2, CGRectGetHeight(self.view.frame)/2);
        self.activityIndicator.hidden=YES;
    }
    return activityIndicator;
}

- (void)goBack:(NSInteger)type{
    if(type==0){
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        if ([self.webView canGoBack]) {
            //如果有则返回
            [self.webView goBack];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)webViewStartLoading{
    [self.activityIndicator startAnimating];
    self.activityIndicator.hidden=NO;
}

- (void)webViewFinishLoading{
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden=YES;
}

- (void)webViewError:(NSError *)error{
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden=YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
