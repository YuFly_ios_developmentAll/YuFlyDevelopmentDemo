//
//  WebViewMainVC.h
//  DSBridgeTest
//
//  Created by YuFly on 2018/2/1.
//  Copyright © 2018年 YuFly. All rights reserved.
//

#import "BaseMainViewController.h"

@interface WebViewMainVC : BaseMainViewController


/**
 通过url加载webview

 @param urlstring url
 @return    回调
 */
- (instancetype)initWithURLString:(NSString *)urlstring;


/**
 通过html文件加载webview

 @param name 文件名字
 @param type 文件类型
 @return 回调
 */
- (instancetype)initWithResource:(NSString *)name
                          ofType:(NSString *)type;
@end
