//
//  JSObject.m
//  DSBridgeTest
//
//  Created by YuFly on 2018/2/1.
//  Copyright © 2018年 YuFly. All rights reserved.
//

#import "JSObject.h"
@interface JSObject ()
@end
@implementation JSObject

/**
 js获取原生数据
 @param dic js传过来的参数
 @return 回调给js的数据
 */
- (NSString *)testMethod:(NSDictionary *)dic{
    return @"这是测试方法";
}


/**
 js返回按钮调用

 @param dic 键值对  key：goBackType  value：0 控制器返回 1 返回到web前一页
 */
- (void)goBack:(NSDictionary *)dic{
    NSInteger type = [dic[@"goBackType"] integerValue]?[dic[@"goBackType"] integerValue]:0;
    if(self.goBack)
        self.goBack(type);
}

@end
