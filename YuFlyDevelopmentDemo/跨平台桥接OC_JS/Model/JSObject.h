//
//  JSObject.h
//  DSBridgeTest
//
//  Created by YuFly on 2018/2/1.
//  Copyright © 2018年 YuFly. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^GoBackBlock)(NSInteger);
@interface JSObject : NSObject
@property (copy, nonatomic)GoBackBlock goBack;
@end
