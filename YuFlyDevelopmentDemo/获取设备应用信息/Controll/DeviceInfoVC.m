//
//  DeviceInfoVC.m
//  YuFlyDevelopmentDemo
//
//  Created by YuFly on 2018/2/5.
//  Copyright © 2018年 YuFly. All rights reserved.
//

#import "DeviceInfoVC.h"
#import "DeviceInfoManager.h"
@interface DeviceInfoVC ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic)UITableView *                              tb;
@property (strong, nonatomic)NSMutableArray *                           dataArray;
@end

@implementation DeviceInfoVC

- (NSMutableArray *)dataArray{
    if(!_dataArray){
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self initNavigationWithLeftBtnBgImg:[UIImage imageNamed:@"返回白@2x.png"] LeftBtnTitle:@"返回" CenterTitle:@"获取设备信息"];
    [self getData];
    [self.view addSubview:self.tb];
}

- (void)getData{
    [self.dataArray addObject:[[DeviceInfoManager sharedManager] getDeviceUUIDString]];
    [self.dataArray addObject:[[DeviceInfoManager sharedManager] getIphoneName]];
    [self.dataArray addObject:[[DeviceInfoManager sharedManager] getDeviceName]];
    [self.dataArray addObject:[[DeviceInfoManager sharedManager] getLocalizedModel]];
    [self.dataArray addObject:[[DeviceInfoManager sharedManager] getDeviceVersion]];
    [self.dataArray addObject:[[DeviceInfoManager sharedManager] getDeviceType]];
    [self.dataArray addObject:[[DeviceInfoManager sharedManager] getAppVersion]];
    [self.dataArray addObject:[[DeviceInfoManager sharedManager] getAppName]];
    [self.dataArray addObject:[[DeviceInfoManager sharedManager] getDeviceCarrierName]];
    [self.dataArray addObject:[NSString stringWithFormat:@"设备电量:%f",[[DeviceInfoManager sharedManager] getCurrentBatteryLevel]]];
    [self.dataArray addObject:[[DeviceInfoManager sharedManager] getDeviceLanguage]];
    [self.dataArray addObject:[[DeviceInfoManager sharedManager] getBatteryState]];
    [self.dataArray addObject:[NSString stringWithFormat:@"设备内存大小:%lld",[[DeviceInfoManager sharedManager] getTotalMemorySize]]];
    [self.dataArray addObject:[NSString stringWithFormat:@"设备可用内存%f",[[DeviceInfoManager sharedManager] getAvailableMemory]]];
    [self.dataArray addObject:[NSString stringWithFormat:@"应用占用内存:%f",[[DeviceInfoManager sharedManager] getAppUsedMemory]]];
    [self.dataArray addObject:[NSString stringWithFormat:@"设备ip:%@",[[DeviceInfoManager sharedManager] getIPAddress:YES]]];
    [self.dataArray addObject:[NSString stringWithFormat:@"wifi名称:%@",[[DeviceInfoManager sharedManager] getWifiName]]];
    
}

- (UITableView *)tb{
    if(!_tb){
        _tb=[[UITableView alloc]initWithFrame:CGRectMake(0,64, self.view.width, self.view.height-64) style:UITableViewStylePlain];
        _tb.backgroundColor=RGB(255, 255, 255);
        _tb.showsVerticalScrollIndicator=NO;
        _tb.showsHorizontalScrollIndicator=NO;
        _tb.delegate=self;
        _tb.dataSource=self;
        _tb.separatorColor=[UIColor clearColor];
    }
    return _tb;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell==nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    NSString *string = [self.dataArray objectAtIndex:indexPath.row];
    cell.textLabel.text = string;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
