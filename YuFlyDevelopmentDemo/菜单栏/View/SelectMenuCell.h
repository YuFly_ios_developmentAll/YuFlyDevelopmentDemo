//
//  SelectMenuCell.h
//  FXYIM
//
//  Created by YuFly on 17/3/13.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectMenuModel.h"
@interface SelectMenuCell : UITableViewCell
{
    UIImageView *imgView;
}
@property (strong, nonatomic)UIImageView *imgView;
@property (strong, nonatomic)UILabel *line;
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
- (void)setSelectModel:(SelectMenuModel *)models;
@end

