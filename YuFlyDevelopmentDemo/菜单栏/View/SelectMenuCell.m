//
//  SelectMenuCell.m
//  FXYIM
//
//  Created by YuFly on 17/3/13.
//  Copyright © 2017年 YuFly. All rights reserved.
//
#import "SelectMenuCell.h"
#import "SelectMenuModel.h"
@implementation SelectMenuCell
{
    UILabel *titleLabel;
    SelectMenuModel *model;
}
@synthesize imgView;
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        self.backgroundColor=[UIColor clearColor];
        imgView=[[UIImageView alloc]init];
        imgView.contentMode=UIViewContentModeScaleToFill;
        [self.contentView addSubview:imgView];
        titleLabel=[[UILabel alloc]init];
        titleLabel.textColor=[UIColor colorWithRed:67.0/255 green:125.0/255 blue:186.0/255 alpha:1.0];
        titleLabel.textAlignment=NSTextAlignmentLeft;
        titleLabel.font=[UIFont systemFontOfSize:18];
        titleLabel.backgroundColor=[UIColor clearColor];
        titleLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        [self.contentView addSubview:titleLabel];
         self.line=[[UILabel alloc]init];
        self.line.backgroundColor=[UIColor colorWithRed:232/255.0 green:232/255.0 blue:232/255.0 alpha:1.0];
         [self.contentView addSubview:self.line];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    imgView.frame=model.imgFrame;
    titleLabel.frame=model.titleFrame;
    self.line.frame=CGRectMake(0,CGRectGetHeight(self.frame)-1, CGRectGetWidth(self.frame), 1);
}

- (void)setSelectModel:(SelectMenuModel *)models{
    model=models;
    imgView.image=models.img;
    titleLabel.text=models.title;
    
}
@end
