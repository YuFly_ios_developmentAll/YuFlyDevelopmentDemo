//
//  BarSelectMenu.m
//  FXYIM
//
//  Created by YuFly on 17/3/13.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import "BarSelectMenu.h"
#import "BarSelectMenu.h"
#import "SelectMenuCell.h"
#define RADIUS 5
#define WIDTH 15
#define HEIGHT 20
@interface BarSelectMenu ()<UITableViewDelegate,UITableViewDataSource>
{   CGPoint starPoint;
    CGPoint upLeftPoint;
    CGPoint upRightPoint;
    CGPoint downRightPoint;
    CGPoint downLeftPoint;
    NSArray *dataArr;
    NSInteger cellHeight;
    UITableView *tb;
    
}
@end
@implementation BarSelectMenu
@synthesize selectIndex;
- (instancetype)initWithFrame:(CGRect)frame
             andWithDataArray:(NSArray *)dataArray
             andWithCellHeigt:(NSInteger)height
                andWithApartX:(CGFloat)x
                andWithApartY:(CGFloat)y
{
    if(self=[super initWithFrame:frame]){
        self.backgroundColor=[UIColor clearColor];
        self.clipsToBounds=YES;
        starPoint=CGPointMake(x,y);
        upLeftPoint=CGPointMake(0,HEIGHT);;
        upRightPoint=CGPointMake(frame.size.width, upLeftPoint.y);
        downLeftPoint=CGPointMake(upLeftPoint.x, upLeftPoint.y+frame.size.height-HEIGHT);
        downRightPoint=CGPointMake(upRightPoint.x, downLeftPoint.y);
        dataArr=dataArray;
        cellHeight=height;
        [self addSubview:self.tb];
    }
    return self;
}

- (UITableView *)tb{
    if(!tb){
        tb=[[UITableView alloc]initWithFrame:CGRectMake(1,HEIGHT-10,CGRectGetWidth(self.frame)-2, dataArr.count*cellHeight-2) style:UITableViewStylePlain];
        tb.separatorColor=[UIColor clearColor];
        tb.backgroundColor=[UIColor clearColor];
        tb.tableFooterView=[UIView new];
        tb.delegate=self;
        tb.dataSource=self;
        tb.backgroundColor=[UIColor whiteColor];
        tb.scrollEnabled=NO;
        tb.layer.cornerRadius=3;
        tb.bounces=NO;
    }
    return tb;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier=@"cell";
    SelectMenuCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell==nil){
        cell=[[SelectMenuCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    SelectMenuModel *model=[dataArr objectAtIndex:indexPath.row];
    
    [cell setSelectModel:model];
    if(selectIndex!=indexPath.row && self.selectType==SelectTypeRadio){
        cell.imgView.hidden=YES;
    }else{
        cell.imgView.hidden=NO;
    }
    
    if(indexPath.row==dataArr.count-1){
        cell.line.hidden=YES;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tb deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row!=dataArr.count-1){
        selectIndex=indexPath.row;
    }
    [tb reloadData];
    _didSelectBlock(indexPath);
}

// 覆盖drawRect方法，你可以在此自定义绘画和动画
- (void)drawRect:(CGRect)rect{
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetAllowsAntialiasing(context, true);
    CGContextSetLineWidth(context,0.1);  //线宽
    CGContextSetRGBStrokeColor(context, 232.0 / 255.0, 232.0 / 255.0, 232.0 / 255.0, 1.0);
    //利用path进行绘制三角形
    
    CGPoint beginPoint=CGPointMake(upLeftPoint.x+RADIUS, upLeftPoint.y);
    CGPoint endPoint=CGPointMake(starPoint.x-WIDTH, starPoint.y+HEIGHT);
    [self drawStraightLineCGContextRef:context andBeginPoint:beginPoint andWithEndPoint:endPoint];
    
    [self drawTriangleCGContextRef:context andOnePoint:CGPointMake(starPoint.x-WIDTH, starPoint.y+HEIGHT) andTwoPoint:CGPointMake(starPoint.x, starPoint.y) andThreePoint:CGPointMake(starPoint.x + WIDTH, starPoint.y+HEIGHT)];
    
    /*
    beginPoint=CGPointMake(starPoint.x-WIDTH, starPoint.y+HEIGHT);
    endPoint=CGPointMake(starPoint.x, starPoint.y);
    [self drawStraightLineCGContextRef:context andBeginPoint:beginPoint andWithEndPoint:endPoint];
    
    beginPoint=CGPointMake(starPoint.x,  starPoint.y);
    endPoint=CGPointMake(starPoint.x + WIDTH, starPoint.y+HEIGHT);
    [self drawStraightLineCGContextRef:context andBeginPoint:beginPoint andWithEndPoint:endPoint];
    
    beginPoint=CGPointMake(starPoint.x + WIDTH,  starPoint.y+HEIGHT);
    endPoint=CGPointMake(upRightPoint.x-RADIUS, upRightPoint.y);
    [self drawStraightLineCGContextRef:context andBeginPoint:beginPoint andWithEndPoint:endPoint];
    
    beginPoint=CGPointMake(upRightPoint.x-RADIUS, upRightPoint.y);
    endPoint=CGPointMake(upRightPoint.x, upRightPoint.y+RADIUS);
    [self drawArcLineCGContextRef:context andCenterPoint:CGPointMake(upRightPoint.x, upRightPoint.y) andBeginPoint:beginPoint andWithEndPoint:endPoint];
    
    beginPoint=CGPointMake(upRightPoint.x,  upRightPoint.y+RADIUS);
    endPoint=CGPointMake(downRightPoint.x, downRightPoint.y-RADIUS);
    [self drawStraightLineCGContextRef:context andBeginPoint:beginPoint andWithEndPoint:endPoint];
    
    beginPoint=CGPointMake(downRightPoint.x, downRightPoint.y-RADIUS);
    endPoint=CGPointMake(downRightPoint.x-RADIUS, downRightPoint.y);
    [self drawArcLineCGContextRef:context andCenterPoint:CGPointMake(downRightPoint.x, downRightPoint.y) andBeginPoint:beginPoint andWithEndPoint:endPoint];
    
    beginPoint=CGPointMake(downRightPoint.x-RADIUS,  downRightPoint.y);
    endPoint=CGPointMake(downLeftPoint.x+RADIUS, downLeftPoint.y);
    [self drawStraightLineCGContextRef:context andBeginPoint:beginPoint andWithEndPoint:endPoint];
    
    beginPoint=CGPointMake(downLeftPoint.x+RADIUS, downLeftPoint.y);
    endPoint=CGPointMake(downLeftPoint.x, downLeftPoint.y-RADIUS);
    [self drawArcLineCGContextRef:context andCenterPoint:CGPointMake(downLeftPoint.x, downLeftPoint.y) andBeginPoint:beginPoint andWithEndPoint:endPoint];
    
    beginPoint=CGPointMake(downLeftPoint.x,  downLeftPoint.y-RADIUS);
    endPoint=CGPointMake(upLeftPoint.x, upLeftPoint.y+RADIUS);
    [self drawStraightLineCGContextRef:context andBeginPoint:beginPoint andWithEndPoint:endPoint];
    
    beginPoint=CGPointMake(upLeftPoint.x, upLeftPoint.y+RADIUS);
    endPoint=CGPointMake(upLeftPoint.x+RADIUS, upLeftPoint.y);
    [self drawArcLineCGContextRef:context andCenterPoint:CGPointMake(upLeftPoint.x, upLeftPoint.y) andBeginPoint:beginPoint andWithEndPoint:endPoint];
     */
    
    
}

- (void)drawStraightLineCGContextRef:(CGContextRef)context
                       andBeginPoint:(CGPoint)beginPoint
                     andWithEndPoint:(CGPoint)endPoint{
    CGContextBeginPath(context);//标记
    CGContextMoveToPoint(context, beginPoint.x, beginPoint.y);//设置起点
    CGContextAddLineToPoint(context,endPoint.x, endPoint.y);
    CGContextClosePath(context);
    [[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0] setStroke];
    CGContextDrawPath(context, kCGPathFillStroke);//绘制路径path
    
}
- (void)drawArcLineCGContextRef:(CGContextRef)context
                 andCenterPoint:(CGPoint)centerPoint
                  andBeginPoint:(CGPoint)beginPoint
                andWithEndPoint:(CGPoint)endPoint{
    CGContextBeginPath(context);//标记
    CGContextMoveToPoint(context, beginPoint.x, beginPoint.y);//开始坐标p1
    CGContextAddArcToPoint(context, centerPoint.x, centerPoint.y, endPoint.x, endPoint.y, RADIUS);
    
    CGContextStrokePath(context);//绘画路径
    
}
- (void)drawTriangleCGContextRef:(CGContextRef)context
                  andOnePoint:(CGPoint)onePoint
                   andTwoPoint:(CGPoint)twoPoint
                 andThreePoint:(CGPoint)threePoint{
    CGContextBeginPath(context);//标记
    CGContextMoveToPoint(context, onePoint.x, onePoint.y);//设置起点
    CGContextAddLineToPoint(context,twoPoint.x, twoPoint.y);
    CGContextAddLineToPoint(context,threePoint.x, threePoint.y);
    CGContextClosePath(context);
    [[UIColor clearColor] setStroke];
    [[UIColor whiteColor] setFill]; //设置填充色
    CGContextDrawPath(context, kCGPathFillStroke);//绘制路径path
}
@end
