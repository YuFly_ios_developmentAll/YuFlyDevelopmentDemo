//
//  SelectMenuBackgroudView.h
//  FXYIM
//
//  Created by YuFly on 17/3/13.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BarSelectMenu.h"
typedef void (^SelectMenuBlock)(NSIndexPath *);
@interface SelectMenuBackgroudView : UIView
{
    BarSelectMenu *menu;
}
@property (copy, nonatomic)SelectMenuBlock selectMenuBlock;
@property (strong, nonatomic)BarSelectMenu *menu;

- (instancetype)initWithMenuRect:(CGRect)rect
                    andDataArray:(NSArray *)dataArr
                   andCellHeight:(CGFloat)height
                        andPoint:(CGPoint)point;

- (void)menuViewpop;
@end
