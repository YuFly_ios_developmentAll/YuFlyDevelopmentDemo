//
//  SelectMenuBackgroudView.m
//  FXYIM
//
//  Created by YuFly on 17/3/13.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import "SelectMenuBackgroudView.h"

#import "SelectMenuModel.h"
@interface SelectMenuBackgroudView ()
{
    
    CGFloat cellHeight;
    CGRect menuRect;
}

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, assign) CGPoint origin;
@end

@implementation SelectMenuBackgroudView
@synthesize menu;

- (instancetype)initWithMenuRect:(CGRect)rect
                andDataArray:(NSArray *)dataArr
               andCellHeight:(CGFloat)height
                    andPoint:(CGPoint)point{
    if(self=[super init]){
        self.frame=CGRectMake(0, 0, [[UIApplication sharedApplication].delegate window].bounds.size.width, [[UIApplication sharedApplication].delegate window].bounds.size.height);
        self.backgroundColor=[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.3];
        cellHeight=height;
        menuRect=rect;
        self.dataArr = dataArr;
        self.origin = CGPointMake(point.x, point.y+10);
        [self menuViewpop];
    }
    return self;
}

- (void)menuViewpop{
    __weak typeof(self) weakself=self;
    UIWindow *window=[[UIApplication sharedApplication].delegate window];
    [window addSubview:self];
    menu=[[BarSelectMenu alloc]initWithFrame:menuRect andWithDataArray:self.dataArr andWithCellHeigt:cellHeight andWithApartX:self.origin.x-menuRect.origin.x andWithApartY:0];
    [self addSubview:menu];
    menu.didSelectBlock=^(NSIndexPath *index){
        weakself.selectMenuBlock(index);
            [weakself menuViewDismiss];
    };
    
    //动画效果弹出
    self.alpha = 0;
    CGRect frame = menu.frame;
    menu.frame = CGRectMake(self.origin.x, self.origin.y, 0, 0);
    [UIView animateWithDuration:0.2 animations:^{
        weakself.alpha = 1;
        weakself.menu.frame = frame;
    }];
}

- (void)menuViewDismiss {
    //动画效果淡出
    __weak typeof(self) weakself=self;
    [UIView animateWithDuration:0.2 animations:^{
        weakself.alpha = 0;
        weakself.menu.frame = CGRectMake(self.origin.x, self.origin.y-10, 0, 0);
    } completion:^(BOOL finished) {
        if (finished) {
            [weakself removeFromSuperview];
        }
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if (![touch.view isEqual:menu]) {
        [self menuViewDismiss];
    }
}
@end
