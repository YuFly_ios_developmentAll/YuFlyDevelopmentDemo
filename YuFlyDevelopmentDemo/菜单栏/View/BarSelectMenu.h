//
//  BarSelectMenu.h
//  FXYIM
//
//  Created by YuFly on 17/3/13.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^DidSelectBlock)(NSIndexPath *);
typedef NS_ENUM(NSUInteger,SelectType) {
    SelectTypeAllType = 1,
    SelectTypeRadio
};
@interface BarSelectMenu : UIView{
    NSInteger selectIndex;
}
@property (copy, nonatomic)DidSelectBlock didSelectBlock;
@property (assign, nonatomic)SelectType selectType;
@property (assign, nonatomic)NSInteger selectIndex;
- (instancetype)initWithFrame:(CGRect)frame
             andWithDataArray:(NSArray *)dataArray
             andWithCellHeigt:(NSInteger)height
                andWithApartX:(CGFloat)x
                andWithApartY:(CGFloat)y;
@end
