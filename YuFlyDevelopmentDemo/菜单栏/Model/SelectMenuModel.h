//
//  SelectMenuModel.h
//  FXYIM
//
//  Created by YuFly on 17/3/13.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface SelectMenuModel : NSObject
@property (strong, nonatomic)UIImage *img;
@property (copy, nonatomic)NSString *title;
@property (assign, nonatomic)CGRect imgFrame;
@property (assign, nonatomic)CGRect titleFrame;
- (instancetype)init;
@end

