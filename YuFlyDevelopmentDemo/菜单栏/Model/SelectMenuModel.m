//
//  SelectMenuModel.m
//  FXYIM
//
//  Created by YuFly on 17/3/13.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import "SelectMenuModel.h"
@implementation SelectMenuModel
- (instancetype)init{
    if(self=[super init]){
        self.img=nil;
        self.title=@"";
        self.imgFrame=CGRectMake(15, 5, 40, 40);
        self.titleFrame=CGRectMake(70, 0, 90, 50);
    }
    return self;
}
@end
