//
//  SelectedMenuMainVC.m
//  YuFlyDevelopmentDemo
//
//  Created by YuFly on 17/8/17.
//  Copyright © 2017年 YuFly. All rights reserved.
//

#import "SelectedMenuMainVC.h"

#import "SelectMenuModel.h"
#import "SelectMenuBackgroudView.h"

@interface SelectedMenuMainVC ()

@end

@implementation SelectedMenuMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initNavigationWithLeftBtnBgImg:[UIImage imageNamed:@"返回白.png"] LeftBtnTitle:@"返回" CenterTitle:@"菜单栏" RightBtnBgImg:[UIImage imageNamed:@"加号"]];
    
}

- (void)rightBtnClick:(UIButton *)btn{
    SelectMenuModel *modelOne=[[SelectMenuModel alloc]init];
    modelOne.img=[UIImage imageNamed:@"蓝色打钩66.png"];
    modelOne.title=@"所有通知";
    modelOne.imgFrame=CGRectMake(20,15, 20, 20);
    modelOne.titleFrame=CGRectMake(50, 0, 100, 50);
    SelectMenuModel *modelTwo=[[SelectMenuModel alloc]init];
    modelTwo.img=[UIImage imageNamed:@"蓝色打钩66.png"];
    modelTwo.title=@"我收到的";
    modelTwo.imgFrame=CGRectMake(20,15, 20, 20);
    modelTwo.titleFrame=CGRectMake(50, 0, 100, 50);
    SelectMenuModel *modelThree=[[SelectMenuModel alloc]init];
    modelThree.img=[UIImage imageNamed:@"蓝色打钩66.png"];
    modelThree.title=@"我发出的";
    modelThree.imgFrame=CGRectMake(20,15, 20, 20);
    modelThree.titleFrame=CGRectMake(50, 0, 100, 50);
    SelectMenuModel *modelFour=[[SelectMenuModel alloc]init];
    modelFour.img=[UIImage imageNamed:@"蓝色打钩66.png"];
    modelFour.title=@"未读通知";
    modelFour.imgFrame=CGRectMake(20,15, 20, 20);
    modelFour.titleFrame=CGRectMake(50, 0, 100, 50);
    SelectMenuModel *modelFive=[[SelectMenuModel alloc]init];
    modelFive.img=[UIImage imageNamed:@"蓝色打钩66.png"];
    modelFive.title=@"发通知";
    modelFive.imgFrame=CGRectMake(20,15, 20, 20);
    modelFive.titleFrame=CGRectMake(50, 0, 100, 50);
    NSArray *menudDataArr=@[modelOne,modelTwo,modelThree,modelFour,modelFive];
    
    SelectMenuBackgroudView *selectMenu=[[SelectMenuBackgroudView alloc]initWithMenuRect:CGRectMake(self.view.width-160, 64, 150, menudDataArr.count*50+15) andDataArray:menudDataArr andCellHeight:50 andPoint:CGPointMake(self.view.width-27, 64)];
    selectMenu.menu.selectType=SelectTypeRadio;
    selectMenu.menu.selectIndex=0;
    selectMenu.selectMenuBlock=^(NSIndexPath *index){
       
        
    };

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
